package com.slipperyslickproductiondesign.fragments;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.slipperyslickproductiondesign.R;
import com.slipperyslickproductiondesign.constant.NetworkConnector;
import com.slipperyslickproductiondesign.fragments.FragmentPlayList.AsyncGetGenreTask;

public class FragmentDiscover extends Fragment {

	Context mContext;
	ListView lv;
	ProgressDialog pDialog;
	DiscoverAdapter discoverAdapter;
	Button btn_geners, btn_music, btn_audio;
	FragmentDiscover fragmentDiscover;

	ArrayList<Integer> discoverimages;

	ArrayList<String> Arrayalbumid;

	ArrayList<String> Arrayalbumsong;
	ArrayList<String> Arrayalbumnames;

	public static int[] images = { R.drawable.secimage, R.drawable.secimage,
			R.drawable.secimage, R.drawable.secimage, R.drawable.secimage,
			R.drawable.secimage, R.drawable.secimage, R.drawable.secimage,
			R.drawable.secimage, R.drawable.secimage, R.drawable.secimage,
			R.drawable.secimage, R.drawable.secimage, R.drawable.secimage,
			R.drawable.secimage };

	public static int[] imagesgeneres = { R.drawable.new2,
			R.drawable.thirdimage, R.drawable.sec, R.drawable.secimage,
			R.drawable.new1, R.drawable.no3, R.drawable.no2 };

	public static int[] imagesmusic = { R.drawable.secimage, R.drawable.new2,
			R.drawable.no3, R.drawable.no2, R.drawable.sec,
			R.drawable.secimage, R.drawable.thirdimage, };

	public static int[] imagesaudio = { R.drawable.no2, R.drawable.new1,
			R.drawable.new2, R.drawable.no3, R.drawable.sec,
			R.drawable.secimage, R.drawable.thirdimage, };

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_discover, container,
				false);
		btn_geners = (Button) view.findViewById(R.id.btn_geners);
		btn_music = (Button) view.findViewById(R.id.btn_music);
		btn_audio = (Button) view.findViewById(R.id.btn_audio);
		btn_geners.setBackground(getResources().getDrawable(
				R.drawable.buton_click_shape));
		btn_geners.setTextColor(Color.WHITE);

		discoverimages = new ArrayList<Integer>();
		fragmentDiscover = new FragmentDiscover();

		Arrayalbumid = new ArrayList<String>();
		Arrayalbumsong = new ArrayList<String>();
		Arrayalbumnames = new ArrayList<String>();

		new AsyncGetGenreDiscover().execute("50");

		discoverAdapter = new DiscoverAdapter(getActivity(), discoverimages,
				Arrayalbumid, Arrayalbumnames, Arrayalbumsong);

		mContext = view.getContext();
		lv = (ListView) view.findViewById(R.id.Listview_discover);
		for (int i = 0; i < images.length; i++) {

			discoverimages.add(images[i]);

			// Log.e("1st", "" + mArrayList.size());
		}

		lv.setAdapter(discoverAdapter);

		btn_geners.setOnClickListener(new OnClickListener() {

			@SuppressLint("ResourceAsColor")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				btn_geners.setBackground(getResources().getDrawable(
						R.drawable.buton_click_shape));
				btn_music.setBackground(getResources().getDrawable(
						R.drawable.button_shape));
				btn_audio.setBackground(getResources().getDrawable(
						R.drawable.button_shape));

				btn_geners.setTextColor(Color.WHITE);

				btn_audio.setTextColor(Color.parseColor("#37AFE7"));

				btn_music.setTextColor(Color.parseColor("#37AFE7"));

				discoverimages.clear();

				for (int i = 0; i < imagesgeneres.length; i++) {

					discoverimages.add(images[i]);

					// Log.e("1st", "" + mArrayList.size());
				}
				discoverAdapter.notifyDataSetChanged();

			}
		});
		btn_music.setOnClickListener(new OnClickListener() {

			@SuppressLint("ResourceAsColor")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				btn_geners.setBackground(getResources().getDrawable(
						R.drawable.button_shape));
				btn_music.setBackground(getResources().getDrawable(
						R.drawable.buton_click_shape));
				btn_audio.setBackground(getResources().getDrawable(
						R.drawable.button_shape));

				btn_geners.setTextColor(Color.parseColor("#37AFE7"));

				btn_audio.setTextColor(Color.parseColor("#37AFE7"));

				btn_music.setTextColor(Color.WHITE);

				discoverimages.clear();

				for (int i = 0; i < imagesmusic.length; i++) {

					discoverimages.add(imagesmusic[i]);

					// Log.e("1st", "" + mArrayList.size());
				}
				discoverAdapter.notifyDataSetChanged();
			}
		});

		btn_audio.setOnClickListener(new OnClickListener() {

			@SuppressLint("ResourceAsColor")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				btn_geners.setBackground(getResources().getDrawable(
						R.drawable.button_shape));
				btn_music.setBackground(getResources().getDrawable(
						R.drawable.button_shape));
				btn_audio.setBackground(getResources().getDrawable(
						R.drawable.buton_click_shape));
				btn_geners.setTextColor(Color.parseColor("#37AFE7"));

				btn_audio.setTextColor(Color.WHITE);

				btn_music.setTextColor(Color.parseColor("#37AFE7"));

				discoverimages.clear();

				for (int i = 0; i < imagesaudio.length; i++) {

					discoverimages.add(imagesaudio[i]);

					// Log.e("1st", "" + mArrayList.size());
				}
				discoverAdapter.notifyDataSetChanged();
			}

		});

		return view;

	}

	class DiscoverAdapter extends BaseAdapter {
		Context mContext;

		ArrayList<Integer> discoverimages2 = new ArrayList<Integer>();

		ArrayList<String> albumid = new ArrayList<String>();
		ArrayList<String> albumsongg = new ArrayList<String>();
		ArrayList<String> albumnames = new ArrayList<String>();
		LayoutInflater inflater;
		FragmentPlayer fragmentPlayer;
		FragmentInsidePlalist fragmentInsideAlbum;

		public DiscoverAdapter(Context Context,
				ArrayList<Integer> discoverimages2,
				ArrayList<String> arrayalbumid,
				ArrayList<String> arrayalbumnames,
				ArrayList<String> arrayalbumsong) {
			this.mContext = Context;
			inflater = LayoutInflater.from(mContext);
			this.discoverimages2 = discoverimages2;
			albumid = arrayalbumid;
			albumsongg = arrayalbumsong;
			albumnames = arrayalbumnames;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return discoverimages2.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return discoverimages2.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub

			final ViewHolder holder;
			// Log.e("", "" + ListDataModel.size());
			if (convertView == null) {
				holder = new ViewHolder();

				convertView = inflater.inflate(
						R.layout.discover_generes_list_items, null);

				holder.frame_generes = (FrameLayout) convertView
						.findViewById(R.id.frame_generes);
				holder.text_no_played = (TextView) convertView
						.findViewById(R.id.text_no_played);
				holder.text_songdetail = (TextView) convertView
						.findViewById(R.id.text_songdetail);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {
				holder.frame_generes.setBackgroundResource(discoverimages2
						.get(position));

				holder.text_songdetail.setText(albumnames.get(position));

				Log.e("1st", "" + albumnames);
				holder.text_no_played.setText(albumsongg.get(position));

				holder.frame_generes.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						fragmentPlayer = new FragmentPlayer();
						fragmentInsideAlbum = new FragmentInsidePlalist();

						// fragmentTransaction.replace(R.id.fragmentmainplace,
						// fragmentPlayer);

						FragmentManager fm = getActivity()
								.getSupportFragmentManager();
						FragmentTransaction fragmentTransaction = fm
								.beginTransaction();
						fragmentTransaction.replace(R.id.fragmentmainplace,
								fragmentInsideAlbum);
						Bundle bundlle = new Bundle();

						bundlle.putString("id", albumid.get(position));

						bundlle.putString("selectedAlbum",
								albumnames.get(position));
						// Log.e("name",""+albumnames.get(position));

						fragmentInsideAlbum.setArguments(bundlle);
						fragmentTransaction.addToBackStack(null);
						fragmentTransaction.commit();

					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
			return convertView;
		}

		public class ViewHolder {
			FrameLayout frame_generes;
			TextView text_no_played, text_songdetail;

		}
	}

	public class AsyncGetGenreDiscover extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(getActivity(), R.style.progress_dialog);

			pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pDialog.dismiss();

			JsonReponseParsing(result);

		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("limit_genre", String
					.valueOf(params[0])));
			String url = NetworkConnector.GetGenre(parameter);
			return url;
		}
	}

	public void JsonReponseParsing(String result) {
		JSONObject jObj;
		try {
			if (result != null) {
				jObj = new JSONObject(result);
				Log.e("response", "" + result);
				if (jObj != null) {
					if (jObj.getBoolean("success")) {

						JSONArray array = jObj.getJSONArray("data");

						for (int i = 0; i < array.length(); i++) {

							JSONObject object = array.getJSONObject(i);

							// Log.e("id", "" + object.getString("id"));
							// Log.e("name", "" + object.getString("name"));
							// Log.e("songsCount", "" +
							// object.getString("songscount"));
							Arrayalbumid.add(object.getString("id"));
							Arrayalbumnames.add(object.getString("name"));
							Arrayalbumsong.add(object.getString("songscount")
									+ " Tracks");
							Log.e("Arrayalbumid", "" + Arrayalbumid);
							Log.e("Arrayalbumnames", "" + Arrayalbumnames);
							Log.e("Arrayalbumsong", "" + Arrayalbumsong);

							// JSONArray array1 = object.getJSONArray("songs");

							// for (int j = 0; j < array1.length(); j++) {
							// JSONObject object1 = array1.getJSONObject(j);
							// listOfAlbums.add(object1);
							// }
							//
							// Log.e("listOfAlbums", "" + listOfAlbums);

						}
						discoverAdapter.notifyDataSetChanged();
					} else {

						showAlertDialog(getActivity(), "ERROR", "Wrong Email",
								false);

					}
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showAlertDialog(Context context, String title, String message,
			boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
