package com.slipperyslickproductiondesign.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.facebook.AccessToken;
import com.slipperyslickproductiondesign.R;
import com.slipperyslickproductiondesign.login.ChossingScreen;

public class FragmentSetting extends Fragment {
	FrameLayout signout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_setting, container,
				false);
		signout = (FrameLayout) view.findViewById(R.id.layout_signout);
		signout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				SharedPreferences settings = getActivity()
						.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
				settings.edit().clear().commit();
				// LoginManager.getInstance().logOut();
				// AccessToken accessToken =
				// AccessToken.getCurrentAccessToken();
				// if(accessToken != null){
				// LoginManager.getInstance().logOut();
				// }
				Intent it = new Intent(getActivity(), ChossingScreen.class);
				getActivity().finish();
				startActivity(it);
			}
		});
		return view;

	}
}
