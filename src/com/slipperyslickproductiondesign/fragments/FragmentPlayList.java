package com.slipperyslickproductiondesign.fragments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.slipperyslickproductiondesign.R;
import com.slipperyslickproductiondesign.constant.NetworkConnector;

public class FragmentPlayList extends Fragment {
	Context mContext;
	ListView lv;
	EditText edit_search;
	String search;
	ImageButton img_search;
	AlbumAdapter albumAdapter;
	ProgressDialog pDialog;
	ArrayList<String> Arrayalbumnames;
	ArrayList<Integer> Arrayalbumimages;
	ArrayList<String> Arrayalbumid;
	ArrayList<String> Arrayalbumsong;
	ArrayList<String> Allsong;
	// List<List<String>> listOfAlbums = new ArrayList<List<String>>();

	ArrayList<JSONObject> listOfAlbums;

	// public static String[] albumnames = { "Popular tracks", "Top Beats",
	// "Dance", "Disco", "Drum & BAss", "Folk", "Hip Hop" };
	//
	// public static int[] albumimages = { R.drawable.artist1,
	// R.drawable.artist2,
	// R.drawable.artist3, R.drawable.artist4, R.drawable.artist3,
	// R.drawable.artist2, R.drawable.artist1 };
	//
	String limit_genre;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_playlist, container,
				false);
		mContext = view.getContext();

		Arrayalbumimages = new ArrayList<Integer>();
		Arrayalbumid = new ArrayList<String>();
		Arrayalbumsong = new ArrayList<String>();
		Arrayalbumnames = new ArrayList<String>();
		Allsong = new ArrayList<String>();
		lv = (ListView) view.findViewById(R.id.Listview_album);
		edit_search = (EditText) view.findViewById(R.id.edit_search);

		edit_search.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

				albumAdapter.getFilter().filter(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		new AsyncGetGenreTask().execute("50");

		albumAdapter = new AlbumAdapter(getActivity(), Arrayalbumnames,
				Arrayalbumid, Arrayalbumsong);

		// albumAdapter = new AlbumAdapter(getActivity(), listOfAlbums);

		lv.setAdapter(albumAdapter);

		return view;

	}

	class AlbumAdapter extends BaseAdapter {
		Context mContext;
		ArrayList<String> albumnames = new ArrayList<String>();
		ArrayList<Integer> albumimages = new ArrayList<Integer>();
		ArrayList<String> albumid = new ArrayList<String>();
		ArrayList<String> albumsongg = new ArrayList<String>();

		private List<String> originalData = null;
		private List<String> filteredData = null;

		private ItemFilter mFilter = new ItemFilter();

		private LayoutInflater inflater;
		FragmentInsidePlalist fragmentInsideAlbum;

		public AlbumAdapter(FragmentActivity activity,
				ArrayList<String> arrayalbumnames,
				ArrayList<String> arrayalbumid, ArrayList<String> arrayalbumsong) {
			// TODO Auto-generated constructor stub
			mContext = activity;
			albumnames = arrayalbumnames;
			albumid = arrayalbumid;
			albumsongg = arrayalbumsong;
			originalData = arrayalbumnames;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return albumid.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return albumid.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null)
				inflater = (LayoutInflater) mContext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();

				convertView = inflater.inflate(
						R.layout.playlist_listview_items, null);

				holder.text_album = (TextView) convertView
						.findViewById(R.id.text_album);
				holder.text_tracks = (TextView) convertView
						.findViewById(R.id.text_tracks);
				holder.imgalbum = (ImageView) convertView
						.findViewById(R.id.img_album);

				holder.layoutalbum = (RelativeLayout) convertView
						.findViewById(R.id.layoutalbum);

				holder.img_forward_arrow = (ImageView) convertView
						.findViewById(R.id.img_forward_arrow);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {
				holder.text_album.setText(albumnames.get(position));
				holder.text_tracks.setText(albumsongg.get(position));
				//

				// Log.e("1st", "" + albumnames);
				// Log.e("2st", "" + albumsongg);

				// holder.imgalbum.setImageResource(albumimages2.get(position));

				holder.layoutalbum.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						fragmentInsideAlbum = new FragmentInsidePlalist();

						FragmentManager fm = getActivity()
								.getSupportFragmentManager();
						FragmentTransaction fragmentTransaction = fm
								.beginTransaction();
						fragmentTransaction.replace(R.id.fragmentmainplace,
								fragmentInsideAlbum);
						Bundle bundlle = new Bundle();

						bundlle.putString("selectedAlbum",
								albumid.get(position));
						fragmentInsideAlbum.setArguments(bundlle);
						fragmentTransaction.addToBackStack(null);
						fragmentTransaction.commit();
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
			return convertView;
		}

		public Filter getFilter() {
			// TODO Auto-generated method stub
			return mFilter;
		}

		private class ItemFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {

				String filterString = constraint.toString().toLowerCase();
				Log.e("filterString", "" + filterString);

				FilterResults results = new FilterResults();

				final List<String> list = originalData;

				Log.e("originalData", "" + originalData);
				int count = list.size();
				final ArrayList<String> nlist = new ArrayList<String>(count);

				String filterableString;
				for (int i = 0; i < count; i++) {
					filterableString = list.get(i);
					if (filterableString.toLowerCase().contains(filterString)) {
						nlist.add(filterableString);

					}
				}
				results.values = nlist;
				results.count = nlist.size();

				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				albumnames = (ArrayList<String>) results.values;
				Log.e("albumnames", "" + albumnames);
				notifyDataSetChanged();
			}

		}

		public class ViewHolder {
			ImageView imgalbum, img_forward_arrow;
			TextView text_album, text_tracks;
			RelativeLayout layoutalbum;
		}
	}

	public class AsyncGetGenreTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(getActivity(), R.style.progress_dialog);

			pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pDialog.dismiss();

			JsonReponseParsing(result);

		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("limit_genre", String
					.valueOf(params[0])));
			String url = NetworkConnector.GetGenre(parameter);
			return url;
		}
	}

	public void JsonReponseParsing(String result) {
		JSONObject jObj;
		try {
			if (result != null) {
				jObj = new JSONObject(result);
				Log.e("response", "" + result);
				if (jObj != null) {
					if (jObj.getBoolean("success")) {

						JSONArray array = jObj.getJSONArray("data");

						for (int i = 0; i < array.length(); i++) {

							JSONObject object = array.getJSONObject(i);

							// Log.e("id", "" + object.getString("id"));
							// Log.e("name", "" + object.getString("name"));
							// Log.e("songsCount", "" +
							// object.getString("songscount"));
							Arrayalbumid.add(object.getString("id"));
							Arrayalbumnames.add(object.getString("name"));
							Arrayalbumsong.add(object.getString("songscount")
									+ " Tracks");
							Log.e("Arrayalbumid", "" + Arrayalbumid);
							Log.e("Arrayalbumnames", "" + Arrayalbumnames);
							Log.e("Arrayalbumsong", "" + Arrayalbumsong);

							// JSONArray array1 = object.getJSONArray("songs");

							// for (int j = 0; j < array1.length(); j++) {
							// JSONObject object1 = array1.getJSONObject(j);
							// listOfAlbums.add(object1);
							// }
							//
							// Log.e("listOfAlbums", "" + listOfAlbums);

						}
						albumAdapter.notifyDataSetChanged();
					} else {

						showAlertDialog(getActivity(), "ERROR", "Wrong Email",
								false);

					}
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showAlertDialog(Context context, String title, String message,
			boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
