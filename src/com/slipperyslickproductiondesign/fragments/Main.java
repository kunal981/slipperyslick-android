package com.slipperyslickproductiondesign.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.slipperyslickproductiondesign.R;

public class Main extends FragmentActivity {
	ImageButton imgprofile, imgalbum, imgdiscover, imgsetting;

	FragmentProfile fragmentprofile;
	FragmentPlayList fragmentalbum;
	FragmentDiscover fragementdiscover;
	FragmentSetting fragmentsetting;
	FragmentManager fm;

	int selected_tab = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		imgprofile = (ImageButton) findViewById(R.id.imgprofile);
		// imgalbum = (ImageButton) findViewById(R.id.imgalbum);

		imgdiscover = (ImageButton) findViewById(R.id.imgdiscover);
		imgsetting = (ImageButton) findViewById(R.id.imgsetting);

		fragmentprofile = new FragmentProfile();
		// fragmentalbum = new FragmentPlayList();
		fragementdiscover = new FragmentDiscover();
		fragmentsetting = new FragmentSetting();

		imgprofile.setBackground(getResources().getDrawable(
				R.drawable.profile_hover));

		if (findViewById(R.id.fragmentmainplace) != null) {
			if (savedInstanceState != null) {
				selected_tab = savedInstanceState.getInt("selected", 1);
				return;
			}

			fm = getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fm.beginTransaction();
			fragmentTransaction
					.replace(R.id.fragmentmainplace, fragmentprofile);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();

			fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {

				@Override
				public void onBackStackChanged() {
					// TODO Auto-generated method stub
					Log.e("", "back");

					Call_back_button();
				}
			});

			imgprofile.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					imgprofile.setBackground(getResources().getDrawable(
							R.drawable.profile_hover));
					// imgalbum.setBackground(getResources().getDrawable(
					// R.drawable.platlist));
					imgdiscover.setBackground(getResources().getDrawable(
							R.drawable.discover));
					imgsetting.setBackground(getResources().getDrawable(
							R.drawable.setting));

					FragmentManager fm = getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fm
							.beginTransaction();
					fragmentTransaction.replace(R.id.fragmentmainplace,
							fragmentprofile);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});

			// imgalbum.setOnClickListener(new OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			//
			// // TODO Auto-generated method stub
			//
			// imgprofile.setBackground(getResources().getDrawable(
			// R.drawable.profile));
			// imgalbum.setBackground(getResources().getDrawable(
			// R.drawable.playlist_hover));
			// imgdiscover.setBackground(getResources().getDrawable(
			// R.drawable.discover));
			// imgsetting.setBackground(getResources().getDrawable(
			// R.drawable.setting));
			//
			// FragmentManager fm = getSupportFragmentManager();
			// FragmentTransaction fragmentTransaction = fm
			// .beginTransaction();
			// fragmentTransaction.replace(R.id.fragmentmainplace,
			// fragmentalbum);
			// fragmentTransaction.addToBackStack(null);
			// fragmentTransaction.commit();
			// }
			// });

			imgdiscover.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					imgprofile.setBackground(getResources().getDrawable(
							R.drawable.profile));
					// imgalbum.setBackground(getResources().getDrawable(
					// R.drawable.platlist));
					imgdiscover.setBackground(getResources().getDrawable(
							R.drawable.discover_hover));
					imgsetting.setBackground(getResources().getDrawable(
							R.drawable.setting));

					FragmentManager fm = getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fm
							.beginTransaction();
					fragmentTransaction.replace(R.id.fragmentmainplace,
							fragementdiscover);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});

			imgsetting.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					imgprofile.setBackground(getResources().getDrawable(
							R.drawable.profile));
					// imgalbum.setBackground(getResources().getDrawable(
					// R.drawable.platlist));
					imgdiscover.setBackground(getResources().getDrawable(
							R.drawable.discover));
					imgsetting.setBackground(getResources().getDrawable(
							R.drawable.setting_hover));

					FragmentManager fm = getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fm
							.beginTransaction();
					fragmentTransaction.replace(R.id.fragmentmainplace,
							fragmentsetting);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}
			});

		}

	}

	public void Call_back_button() {

		if (fragmentprofile.isVisible()) {

			selected_tab = 1;

		}
		// else if (fragmentalbum.isVisible()) {
		//
		// selected_tab = 2;

		// }
		else if (fragementdiscover.isVisible()) {

			selected_tab = 3;

		} else if (fragmentsetting.isVisible()) {

			selected_tab = 4;

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// if (fragmentprofile.isVisible()) {
		// finish();
		//
		// }
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Main.this);

		// set title
		alertDialogBuilder.setTitle("Confirm to Exit");

		// set dialog message
		alertDialogBuilder
				.setMessage("Do you want to exit!")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								Main.this.finish();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

}
