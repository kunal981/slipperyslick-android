package com.slipperyslickproductiondesign.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.slipperyslickproductiondesign.R;

public class FragmentPlayer extends Fragment {
	ImageView imgcross, imageview_play, imageview_repeat, imageview_reveres,
			imageview_forward, imageview_suffle, imageview_plus,
			imageview_dots;
	FragmentInsidePlalist fragmentInsideAlbum;
	boolean flag = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// Inflate the layout for this fragment
		View view = inflater
				.inflate(R.layout.fragment_player, container, false);
		imageview_play = (ImageView) view.findViewById(R.id.imageview_play);
		imageview_repeat = (ImageView) view.findViewById(R.id.imageview_repeat);
		imageview_reveres = (ImageView) view
				.findViewById(R.id.imageview_reveres);
		imageview_forward = (ImageView) view
				.findViewById(R.id.imageview_forward);
		imageview_suffle = (ImageView) view.findViewById(R.id.imageview_suffle);
		imageview_plus = (ImageView) view.findViewById(R.id.imageview_plus);
		imageview_play = (ImageView) view.findViewById(R.id.imageview_play);
		imageview_dots = (ImageView) view.findViewById(R.id.imageview_dots);

		imgcross = (ImageView) view.findViewById(R.id.imgcross);

		imageview_play.setBackground(getResources()
				.getDrawable(R.drawable.play));
		imageview_suffle.setBackground(getResources().getDrawable(
				R.drawable.suffle));
		imgcross.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				FragmentManager fm = getActivity().getSupportFragmentManager();
				fm.popBackStack();

			}
		});
		imageview_play.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (flag) {
					imageview_play.setBackground(getResources().getDrawable(
							R.drawable.pause_hover));
					flag = false;
				} else {
					flag = true;
					imageview_play.setBackground(getResources().getDrawable(
							R.drawable.play));
				}

			}
		});
		imageview_suffle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (flag) {
					imageview_suffle.setBackground(getResources().getDrawable(
							R.drawable.suffle_hover));
					flag = false;
				} else {
					flag = true;
					imageview_suffle.setBackground(getResources().getDrawable(
							R.drawable.suffle));
				}

			}
		});

		return view;

	}
}