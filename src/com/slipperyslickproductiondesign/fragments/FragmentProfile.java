package com.slipperyslickproductiondesign.fragments;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.slipperyslickproductiondesign.R;

public class FragmentProfile extends Fragment {
	View redview1, redview2, redview3;
	// TextView text_info, text_posts, text_playlist;
	FrameLayout frame_info, frame_posts, frame_playlist;
	Context mContext;
	ListView lv;
	SharedPreferences pref;
	Editor editor;
	ProfileAdapter profileAdapter;
	TextView name, fullname, useremail;
	ArrayList<Integer> profileimages;

	ArrayList<String> artistname1;
	ArrayList<String> listdetail1;

	public static int[] images = { R.drawable.artist1, R.drawable.artist2,
			R.drawable.artist3, R.drawable.artist4 };

	public static String[] artistname = { "Selena Gomez", "Lucy Hale",
			"Miliey Cryrus", "Jordan" };
	public static String[] listdetail = { "Made With the Flaxen.mp3",
			"Footloose Blue-Ray", "Kenny Loggins Talks", "Blue_Ray" };

	public static int[] imagesplaylist = { R.drawable.artist4,
			R.drawable.artist5, R.drawable.artist6, R.drawable.artist7, };

	public static String[] artistnameplay = { "Miliey Cryrus", "Jordan",
			"Selena Gomez", "Lucy Hale", };

	public static String[] listdetailplay = { "Kenny Loggins Talks",
			"Blue_Ray", "Made With the Flaxen.mp3", "Footloose Blue-Ray", };

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// Inflate the layout for this fragment

		View view = inflater.inflate(R.layout.fragment_profile, container,
				false);
		mContext = view.getContext();
		lv = (ListView) view.findViewById(R.id.Listview_profile);
		name = (TextView) view.findViewById(R.id.text_profile);
		fullname = (TextView) view.findViewById(R.id.text_fullname);
		useremail = (TextView) view.findViewById(R.id.text_email);
		frame_info = (FrameLayout) view.findViewById(R.id.frame_info);
		frame_posts = (FrameLayout) view.findViewById(R.id.frame_posts);
		frame_playlist = (FrameLayout) view.findViewById(R.id.frame_playlist);
		redview1 = (View) view.findViewById(R.id.redview1);
		redview2 = (View) view.findViewById(R.id.redview2);
		redview3 = (View) view.findViewById(R.id.redview3);
		pref = getActivity().getSharedPreferences("MyPref", 0);
		editor = pref.edit();
		name.setText(pref.getString("username", ""));
		fullname.setText(pref.getString("fullname", ""));
		useremail.setText(pref.getString("email", ""));
		artistname1 = new ArrayList<String>();
		listdetail1 = new ArrayList<String>();

		profileimages = new ArrayList<Integer>();

		for (int i = 0; i < images.length; i++) {

			profileimages.add(images[i]);
			artistname1.add(artistname[i]);
			listdetail1.add(listdetail[i]);

			// Log.e("1st", "" + mArrayList.size());
		}

		profileAdapter = new ProfileAdapter(getActivity(), profileimages,
				artistname1, listdetail1);

		lv.setAdapter(profileAdapter);
		frame_info.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				redview1.setVisibility(View.VISIBLE);
				redview2.setVisibility(View.INVISIBLE);
				redview3.setVisibility(View.INVISIBLE);
				lv.setVisibility(View.INVISIBLE);

			}
		});
		frame_posts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				redview1.setVisibility(View.INVISIBLE);
				redview2.setVisibility(View.VISIBLE);
				redview3.setVisibility(View.INVISIBLE);
				lv.setVisibility(View.VISIBLE);
				profileimages.clear();

				for (int i = 0; i < images.length; i++) {

					profileimages.add(images[i]);
					artistname1.add(artistname[i]);
					listdetail1.add(listdetail[i]);

					// Log.e("1st", "" + mArrayList.size());
				}

				profileAdapter.notifyDataSetChanged();

			}
		});
		frame_playlist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				redview1.setVisibility(View.INVISIBLE);
				redview2.setVisibility(View.INVISIBLE);
				redview3.setVisibility(View.VISIBLE);
				lv.setVisibility(View.VISIBLE);

				profileimages.clear();
				for (int i = 0; i < imagesplaylist.length; i++) {

					profileimages.add(imagesplaylist[i]);
					artistname1.add(artistname[i]);
					listdetail1.add(listdetail[i]);

					// Log.e("1st", "" + mArrayList.size());
				}

				profileAdapter.notifyDataSetChanged();
			}
		});

		return view;

	}

	class ProfileAdapter extends BaseAdapter {
		Context mContext;

		FragmentPlayer fragmentPlayer;

		ArrayList<Integer> profileimages2 = new ArrayList<Integer>();
		ArrayList<String> artistname = new ArrayList<String>();
		ArrayList<String> listdetail = new ArrayList<String>();

		LayoutInflater inflater;

		public ProfileAdapter(Context Context,
				ArrayList<Integer> profileimages2,
				ArrayList<String> artistname, ArrayList<String> listdetail) {
			this.mContext = Context;
			inflater = LayoutInflater.from(mContext);
			this.profileimages2 = profileimages2;
			this.artistname = artistname;
			this.listdetail = listdetail;
			// Log.e("", "" + arraylist.size());
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return profileimages2.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return profileimages2.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			final ViewHolder holder;
			// Log.e("", "" + ListDataModel.size());
			if (convertView == null) {
				holder = new ViewHolder();

				convertView = inflater
						.inflate(R.layout.profile_listitems, null);

				holder.frame_list = (FrameLayout) convertView
						.findViewById(R.id.frame_list);

				holder.img_list_profile_song = (ImageView) convertView
						.findViewById(R.id.img_list_profile_song);
				holder.text_list_artistname = (TextView) convertView
						.findViewById(R.id.text_list_artistname);
				holder.text_list_detail = (TextView) convertView
						.findViewById(R.id.text_list_detail);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.img_list_profile_song.setBackgroundResource(profileimages2
					.get(position));
			holder.text_list_artistname.setText(artistname.get(position));
			holder.text_list_detail.setText(listdetail.get(position));

			holder.frame_list.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					fragmentPlayer = new FragmentPlayer();

					FragmentManager fm = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fm
							.beginTransaction();
					fragmentTransaction.replace(R.id.fragmentmainplace,
							fragmentPlayer);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();

				}
			});

			return convertView;
		}

		public class ViewHolder {
			FrameLayout frame_list;
			ImageView img_list_profile_song;
			TextView text_list_artistname, text_list_detail;

		}
	}
}
