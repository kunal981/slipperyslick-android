package com.slipperyslickproductiondesign.fragments;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.slipperyslickproductiondesign.R;
import com.slipperyslickproductiondesign.constant.NetworkConnector;

public class FragmentInsidePlalist extends Fragment {
	ListView lv;
	TextView text_info, text_posts, text_playlist, text_album;
	Context mContext;
	ImageView imgback;
	FragmentPlayList fragmentAlbum;
	ProgressDialog pDialog;
	InsideAlbumAdapter insideAlbumAdapter;
	int id;
	String selectedName;
	ProgressBar progressBar;
	int page = 0;
	ArrayList<Integer> profileimages;
	ArrayList<String> artistname1;
	ArrayList<String> listdetail1;

	ArrayList<String> listdetail;
	ArrayList<String> listduration;
	ArrayList<String> listplaycount;

	ArrayList<String> myListItems;
	boolean loadingMore = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragmentinsideplaylist,
				container, false);
		mContext = view.getContext();
		// View footerView = ((LayoutInflater) getActivity().getSystemService(
		// Context.LAYOUT_INFLATER_SERVICE)).inflate(
		// R.layout.loading_view, null, false);

		// / progressBar = (ProgressBar)
		// footerView.findViewById(R.id.progressbar);

		imgback = (ImageView) view.findViewById(R.id.img_back);
		lv = (ListView) view.findViewById(R.id.listview_insidefragment);
		text_album = (TextView) view.findViewById(R.id.text_album);
		id = Integer.parseInt(getArguments().getString("id"));
		selectedName = getArguments().getString("selectedAlbum");

		Log.e("selectedName", "" + selectedName);
		text_album.setText(selectedName);
		profileimages = new ArrayList<Integer>();
		artistname1 = new ArrayList<String>();
		listdetail1 = new ArrayList<String>();
		listdetail = new ArrayList<String>();
		listduration = new ArrayList<String>();
		listplaycount = new ArrayList<String>();

		// this.lv.addFooterView(footerView);
		// for (int i = 0; i < images.length; i++) {
		//
		// profileimages.add(images[i]);
		// artistname1.add(artistname[i]);
		// listdetail1.add(listdetail[i]);
		//
		// }
		// new AsyncGetGenreSongTask().execute();

		new ExecuteSongRefresh().execute("" + id, "" + listdetail1.size(),
				"" + 80);

		insideAlbumAdapter = new InsideAlbumAdapter(getActivity(), listdetail1);

		lv.setAdapter(insideAlbumAdapter);

		imgback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FragmentManager fm = getActivity().getSupportFragmentManager();
				fm.popBackStack();

			}
		});

		// lv.setOnScrollListener(new AbsListView.OnScrollListener() {
		//
		// @Override
		// public void onScrollStateChanged(AbsListView view, int scrollState) {
		// }
		//
		// @Override
		// public void onScroll(AbsListView view, int firstVisibleItem,
		// int visibleItemCount, int totalItemCount) {
		//
		// // what is the bottom item that is visible
		// int lastInScreen = firstVisibleItem + visibleItemCount;
		//
		// // is the bottom item visible & not loading more already? Load
		// // more!
		// if ((lastInScreen == totalItemCount) && !(loadingMore)) {
		// Log.e("firstVisibleItem", "" + totalItemCount);
		// progressBar.setVisibility(View.VISIBLE);
		// // // start a new thread for loading the items in the list
		// // Thread thread = new Thread(null, loadMoreListItems);
		// // thread.start();
		// }
		//
		// }
		// });
		return view;

	}

	// private Runnable loadMoreListItems = new Runnable() {
	//
	// @Override
	// public void run() {
	//
	// // Set flag so we cant load new items 2 at the same time
	// loadingMore = true;
	// Log.e("loadingMore", "" + loadingMore);
	// // Reset the array that holds the new items
	// myListItems = new ArrayList<String>();
	//
	// // Simulate a delay, delete this on a production environment!
	// try {
	// Thread.sleep(1000);
	//
	// } catch (InterruptedException e) {
	// }
	//
	// // Done! now continue on the UI thread
	// getActivity().runOnUiThread(returnRes);
	//
	// }
	// };
	// private Runnable returnRes = new Runnable() {
	//
	// @Override
	// public void run() {
	//
	// // Tell to the adapter that changes have been made, this will cause
	// // the list to refresh
	//
	// if (page == 0) {
	//
	// progressBar.setVisibility(View.GONE);
	// }
	//
	// new ExecuteSongRefresh().execute("" + id, "" + listdetail1.size(),
	// "" + 10);
	//
	// }
	//
	// };

	class InsideAlbumAdapter extends BaseAdapter {
		Context mContext;

		ArrayList<Integer> insideAlbumimages2 = new ArrayList<Integer>();
		ArrayList<String> artistname = new ArrayList<String>();
		ArrayList<String> listdetail = new ArrayList<String>();

		LayoutInflater inflater;
		FragmentPlayer fragmentPlayer;

		public InsideAlbumAdapter(Context Context, ArrayList<String> artistname) {
			this.mContext = Context;
			inflater = LayoutInflater.from(mContext);
			this.insideAlbumimages2 = insideAlbumimages2;
			this.artistname = artistname;
			this.listdetail = artistname;

			// Log.e("", "" + arraylist.size());
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub

			return listdetail.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return listdetail.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			final ViewHolder holder;
			// Log.e("", "" + ListDataModel.size());
			if (convertView == null) {
				holder = new ViewHolder();

				convertView = inflater
						.inflate(R.layout.profile_listitems, null);

				holder.img_list_profile_song = (ImageView) convertView
						.findViewById(R.id.img_list_profile_song);
				holder.frame_list = (FrameLayout) convertView
						.findViewById(R.id.frame_list);
				holder.text_list_artistname = (TextView) convertView
						.findViewById(R.id.text_list_artistname);

				holder.text_list_detail = (TextView) convertView
						.findViewById(R.id.text_list_detail);

				holder.textplayed = (TextView) convertView
						.findViewById(R.id.textplayed);
				holder.text_time = (TextView) convertView
						.findViewById(R.id.text_time);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			// holder.img_list_profile_song
			// .setBackgroundResource(insideAlbumimages2.get(position));
			// holder.text_list_artistname.setText(artistname.get(position));

			holder.text_list_detail.setText(listdetail.get(position));
			holder.textplayed.setText(listplaycount.get(position));
			holder.text_time.setText(listduration.get(position));
			// Log.e("1st", "" + listduration);

			holder.frame_list.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					fragmentPlayer = new FragmentPlayer();

					FragmentManager fm = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fm
							.beginTransaction();
					fragmentTransaction.replace(R.id.fragmentmainplace,
							fragmentPlayer);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();

				}
			});

			return convertView;
		}

		public class ViewHolder {
			FrameLayout frame_list;
			ImageView img_list_profile_song;
			TextView text_list_artistname, text_list_detail, textplayed,
					text_time;
		}

	}

	// public class AsyncGetGenreSongTask extends AsyncTask<String, Void,
	// String> {
	//
	// @Override
	// protected void onPreExecute() {
	// super.onPreExecute();
	//
	// pDialog = new ProgressDialog(getActivity(), R.style.progress_dialog);
	//
	// pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
	// pDialog.setCancelable(false);
	// pDialog.show();
	//
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	// super.onPostExecute(result);
	// pDialog.dismiss();
	//
	// JsonReponseParsing(result);
	// }
	//
	// @Override
	// protected String doInBackground(String... params) {
	//
	// String result = NetworkConnector.GetGenre();
	// return result;
	// }
	//
	// }
	//
	// public void JsonReponseParsing(String result) {
	// JSONObject jObj;
	// try {
	// jObj = new JSONObject(result);
	// Log.e("response", "" + result);
	// if (jObj != null) {
	// if (jObj.getBoolean("success")) {
	//
	// JSONArray array = jObj.getJSONArray("data");
	//
	// for (int i = id - 1; i < id; i++) {
	//
	// JSONObject object = array.getJSONObject(i);
	// // Arrayalbumid.add(object.getString("id"));
	// // Arrayalbumnames.add(object.getString("name"));
	// // Arrayalbumsong.add(object.getString("songsCount")
	// // + " Tracks");
	// //
	// JSONArray array1 = object.getJSONArray("songs");
	// Log.e("array1", "" + array1);
	// for (int j = 0; j < array1.length(); j++) {
	// JSONObject object1 = array1.getJSONObject(j);
	// listdetail1.add(object1.getString("title"));
	//
	// }
	// }
	// Log.e("listdetail1", "" + listdetail1);
	// insideAlbumAdapter.notifyDataSetChanged();
	// } else {
	//
	// showAlertDialog(getActivity(), "ERROR",
	// jObj.getString("error"), false);
	//
	// }
	// }
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	public class ExecuteSongRefresh extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(getActivity(), R.style.progress_dialog);

			pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pDialog.dismiss();

			JsonReponseParsingsong(result);
		}

		@Override
		protected String doInBackground(String... params) {
			List<NameValuePair> parameter = new LinkedList<NameValuePair>();
			parameter.add(new BasicNameValuePair("genre_id", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("count", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("limit", String
					.valueOf(params[2])));

			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.GetGenresong(parameter);
			return result;
		}

	}

	public void JsonReponseParsingsong(String result) {
		JSONObject jObj;
		try {
			jObj = new JSONObject(result);
			Log.e("response", "" + result);
			if (jObj != null) {
				if (jObj.getBoolean("success")) {

					JSONArray array = jObj.getJSONArray("data");

					// JSONObject object = jObj.getJSONObject("data");

					for (int i = 0; i < array.length(); i++) {

						JSONObject object = array.getJSONObject(i);
						// Arrayalbumid.add(object.getString("id"));
						// Arrayalbumnames.add(object.getString("name"));
						// Arrayalbumsong.add(object.getString("songsCount")
						// + " Tracks");
						//

						// Log.e("listdetail1", "" +
						// object.getString("m_title"));
						// Log.e("listplaycount", "" +
						// object.getString("play_count"));
						// Log.e("listduration", "" +
						// object.getString("play_duration"));
						//
						listdetail1.add(object.getString("m_title"));
						listplaycount.add(object.getString("play_count"));
						listduration.add(object.getString("play_duration"));

					}
					Log.e("listdetail1", "" + listdetail1);
					insideAlbumAdapter.notifyDataSetChanged();
					// loadingMore = false;
					// progressBar.setVisibility(View.GONE);
				} else {

					showAlertDialog(getActivity(), "ERROR",
							jObj.getString("error"), false);
					// loadingMore = false;
					// progressBar.setVisibility(View.GONE);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showAlertDialog(Context context, String title, String message,
			boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}
