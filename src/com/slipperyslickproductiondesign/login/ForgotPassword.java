package com.slipperyslickproductiondesign.login;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.slipperyslickproductiondesign.R;
import com.slipperyslickproductiondesign.constant.ConnectionDetector;
import com.slipperyslickproductiondesign.constant.NetworkConnector;

public class ForgotPassword extends Activity {
	Button btnback, btn_submitemail;
	ProgressDialog pDialog;
	ConnectionDetector mConnectionDetector;
	String email;
	String randomcode;
	EditText editext_forgotpassword;

	public static String[] codes = { "6549", "2453", "5478", "8961", "5691",
			"1928" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgot_password);
		editext_forgotpassword = (EditText) findViewById(R.id.editext_forgotpassword_email);
		mConnectionDetector = new ConnectionDetector(this);
		btn_submitemail = (Button) findViewById(R.id.btn_submitemail);
		btnback = (Button) findViewById(R.id.button_forgot_back);

		btnback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent it = new Intent(ForgotPassword.this, LoginScreen.class);
				startActivity(it);
				finish();

			}
		});

		btn_submitemail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// Log.e("random", "" + random);
				email = editext_forgotpassword.getText().toString();

				int code = new Random().nextInt(codes.length);
				randomcode = (codes[code]);
				if (mConnectionDetector.isConnectingToInternet()) {

					ForgotPasswordProcess();
				} else {
					showAlertDialog(ForgotPassword.this, "Error",
							"No Internet Connection", false);

				}

			}
		});

	}

	public class AsyncFrogotPasswordTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(ForgotPassword.this,
					R.style.progress_dialog);

			pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pDialog.dismiss();

			JsonReponseParsing(result);
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("email", String
					.valueOf(params[0])));

			parameter.add(new BasicNameValuePair("token", String
					.valueOf(params[1])));

			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.ForgotPassword(parameter);
			return result;
		}

	}

	public void JsonReponseParsing(String result) {
		JSONObject jObj;
		try {
			jObj = new JSONObject(result);
			Log.e("response", "" + result);
			if (jObj != null) {
				if (jObj.getBoolean("success")) {

					Intent intent = new Intent(ForgotPassword.this,
							ConfirmCode.class);
					intent.putExtra("Email", email);
					intent.putExtra("Code", randomcode);
					startActivity(intent);
					finish();

				} else {

					showAlertDialog(ForgotPassword.this, "ERROR",
							jObj.getString("error"), false);

				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected boolean validation(String stremail) {
		// TODO Auto-generated method stub
		if (stremail.equals("")) {

			showAlertDialog(ForgotPassword.this, "Error",
					"Please enter your Email", false);

			return false;
		}

		return true;
	}

	public void ForgotPasswordProcess() {

		boolean receive = validation(email);
		if (receive) {

			new AsyncFrogotPasswordTask().execute(email, randomcode);
		}

	}

	public void showAlertDialog(Context context, String title, String message,
			boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}
