package com.slipperyslickproductiondesign.login;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.slipperyslickproductiondesign.R;
import com.slipperyslickproductiondesign.constant.ConnectionDetector;
import com.slipperyslickproductiondesign.constant.NetworkConnector;

public class RegisterScreen extends Activity {
	Button btnback, button_submit;
	String firstname, lastname, email, password, confirmpassword, username;
	EditText editext_firstname, editext_lastname, editext_email,
			editext_password, editext_confirm_password, editext_username;
	ProgressDialog pDialog;
	ConnectionDetector mConnectionDetector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_screeen);
		button_submit = (Button) findViewById(R.id.button_submit);
		editext_firstname = (EditText) findViewById(R.id.editext_firstname);
		editext_lastname = (EditText) findViewById(R.id.editext_lastname);
		editext_email = (EditText) findViewById(R.id.editext_email);
		editext_password = (EditText) findViewById(R.id.editext_password);
		editext_confirm_password = (EditText) findViewById(R.id.editext_confirm_password);
		editext_username = (EditText) findViewById(R.id.editext_username);

		mConnectionDetector = new ConnectionDetector(this);

		btnback = (Button) findViewById(R.id.button_back);
		btnback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent it = new Intent(RegisterScreen.this,
						ChossingScreen.class);

				startActivity(it);
				finish();

			}
		});
		button_submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				firstname = editext_firstname.getText().toString();
				lastname = editext_lastname.getText().toString();
				email = editext_email.getText().toString();
				password = editext_password.getText().toString();
				confirmpassword = editext_confirm_password.getText().toString();
				username = editext_username.getText().toString();

				if (mConnectionDetector.isConnectingToInternet()) {
					RegisterProcess();
				}

				else {

					showAlertDialog(RegisterScreen.this, "Error",
							"No Internet Connection ", false);
				}
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent it = new Intent(RegisterScreen.this, ChossingScreen.class);

		startActivity(it);
		finish();
	}

	public class AsyncRegisterTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(RegisterScreen.this,
					R.style.progress_dialog);

			pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pDialog.dismiss();

			JsonReponseParsing(result);
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("firstname", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("lastname", String
					.valueOf(params[1])));
			parameter.add(new BasicNameValuePair("email", String
					.valueOf(params[2])));
			parameter.add(new BasicNameValuePair("password", String
					.valueOf(params[3])));
			parameter.add(new BasicNameValuePair("username", String
					.valueOf(params[4])));
			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.Register(parameter);
			return result;
		}

	}

	public void JsonReponseParsing(String result) {
		JSONObject jObj;
		try {
			jObj = new JSONObject(result);

			Log.e("response", "" + result);

			if (jObj != null) {
				if (jObj.getBoolean("success")) {

					Intent it = new Intent(RegisterScreen.this,
							LoginScreen.class);
					startActivity(it);
					finish();

				} else {

					JSONObject phone = new JSONObject(result);
					Log.e("object", "" + phone);

					String Server_message = phone.getString("error");

					Log.e("Server_message", "" + Server_message);

					showAlertDialog(RegisterScreen.this, "Failed",
							Server_message, false);

					// showAlertDialog(RegisterScreen.this, "Failed",
					// "Registration Failed", false);

				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected boolean validation(String strfirstname, String strlastname,
			String stremail, String strpassword, String strconfirmpassword,
			String strusername) {
		// TODO Auto-generated method stub
		if (strfirstname.equals("") && strlastname.equals("")
				&& stremail.equals("") && strpassword.equals("")
				&& strconfirmpassword.equals("")) {

			showAlertDialog(RegisterScreen.this, "Feild Vaccent",
					"Please enter all Feilds", false);
			return false;

		}

		if (strfirstname.equals("")) {

			showAlertDialog(RegisterScreen.this, "Feild Vaccent",
					"Please enter FirstName", false);

			return false;
		}
		if (strlastname.equals("")) {

			showAlertDialog(RegisterScreen.this, "Feild Vaccent",
					"Please enter Lastname", false);

			return false;
		}
		if (strusername.equals("")) {

			showAlertDialog(RegisterScreen.this, "Feild Vaccent",
					"Please enter Username", false);

			return false;
		}

		if (stremail.equals("")) {
			showAlertDialog(RegisterScreen.this, "Feild Vaccent",
					"Please enter Email", false);

			return false;
		}
		if (strpassword.equals("")) {

			showAlertDialog(RegisterScreen.this, "Feild Vaccent",
					"Please enter Password", false);

			return false;
		}
		if (strconfirmpassword.equals("")) {

			showAlertDialog(RegisterScreen.this, "Feild Vaccent",
					"Please confirm password", false);

			return false;
		}

		if (strpassword.equals(strconfirmpassword)) {

			return true;

		}

		if (!strpassword.equals(strconfirmpassword)) {

			showAlertDialog(RegisterScreen.this, "Error",
					"Password Does not match", false);
			return false;

		}

		return true;
	}

	public void RegisterProcess() {

		boolean receive = validation(firstname, lastname, email, password,
				confirmpassword, username);
		if (receive) {
			new AsyncRegisterTask().execute(firstname, lastname, email,
					password, username);
		}

	}

	public void showAlertDialog(Context context, String title, String message,
			boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}
