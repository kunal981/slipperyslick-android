package com.slipperyslickproductiondesign.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.slipperyslickproductiondesign.R;
import com.slipperyslickproductiondesign.constant.ConnectionDetector;

public class ConfirmCode extends Activity {

	Button btnback, btn_submitcode;

	ConnectionDetector mConnectionDetector;
	String email, code;
	EditText editext_code;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.confirmcode);
		Intent intent = getIntent();
		email = intent.getStringExtra("Email");
		code = intent.getStringExtra("Code");
		btn_submitcode = (Button) findViewById(R.id.btn_submitcode);
		btnback = (Button) findViewById(R.id.button_forgot_back);
		editext_code = (EditText) findViewById(R.id.editext_forgotpassword_code);
		btnback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent it = new Intent(ConfirmCode.this, ForgotPassword.class);
				startActivity(it);
				finish();
			}

		});
		btn_submitcode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (editext_code.getText().toString().equals("")) {
					showAlertDialog(ConfirmCode.this, "ERROR",
							"Please insert code", false);
				} else if (code.equals(editext_code.getText().toString())) {
					Intent intent = new Intent(ConfirmCode.this,
							ChangePassword.class);
					intent.putExtra("Email", email);

					startActivity(intent);
					finish();
				} else {
					showAlertDialog(ConfirmCode.this, "ERROR",
							"Please insert valid code", false);
				}
			}
		});

	}

	public void showAlertDialog(Context context, String title, String message,
			boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}
