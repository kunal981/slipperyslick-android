package com.slipperyslickproductiondesign.login;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.VideoView;

import com.slipperyslickproductiondesign.R;

public class ChossingScreen extends Activity {
	VideoView mVideoView;
	Button btn_login, btn_signup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_screen);
		btn_login = (Button) findViewById(R.id.btn_login);
		btn_signup = (Button) findViewById(R.id.btn_signup);

		getWindow().setFormat(PixelFormat.UNKNOWN);

		mVideoView = (VideoView) findViewById(R.id.VideoView);

		Uri uriPath = Uri.parse("android.resource://" + getPackageName() + "/"
				+ R.raw.girl);

		mVideoView.setVideoURI(uriPath);

		mVideoView.requestFocus();
		mVideoView.start();

		btn_login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent it = new Intent(ChossingScreen.this, LoginScreen.class);
				startActivity(it);
				finish();
			}
		});
		btn_signup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent it = new Intent(ChossingScreen.this,
						RegisterScreen.class);

				startActivity(it);
				finish();
			}
		});
	}

}
