package com.slipperyslickproductiondesign.login;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.VideoView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.UserInfoChangedCallback;
import com.slipperyslickproductiondesign.R;
import com.slipperyslickproductiondesign.constant.ConnectionDetector;
import com.slipperyslickproductiondesign.constant.NetworkConnector;
import com.slipperyslickproductiondesign.fragments.Main;

public class LoginScreen extends Activity {
	Button btn_back, btn_login;
	FrameLayout frame_forgotpassword;
	EditText edit_username, edit_password;
	String username, password;
	ConnectionDetector mConnectionDetector;
	ProgressDialog pDialog;
	Editor editor;
	VideoView mVideoView;
	SharedPreferences sharedPreferences;
	private LoginButton loginBtn;

	private UiLifecycleHelper uiHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_in_screen);

		SharedPreferences sharedPreferences = getApplicationContext()
				.getSharedPreferences("MyPref", MODE_PRIVATE);
		editor = sharedPreferences.edit();
		mConnectionDetector = new ConnectionDetector(this);
		getWindow().setFormat(PixelFormat.UNKNOWN);

		mVideoView = (VideoView) findViewById(R.id.VideoViewlogin);

		Uri uriPath = Uri.parse("android.resource://" + getPackageName() + "/"
				+ R.raw.girl);
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.facebook.samples.loginhowto",
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("KeyHash:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
			Log.e("CRASH", "" + e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			Log.e("CRASh", "" + e.getMessage());
		}
		mVideoView.setVideoURI(uriPath);

		mVideoView.requestFocus();
		mVideoView.start();

		uiHelper = new UiLifecycleHelper(this, statusCallback);
		uiHelper.onCreate(savedInstanceState);

		loginBtn = (LoginButton) findViewById(R.id.fb_login_button);
		loginBtn.setReadPermissions(Arrays.asList("email"));
		loginBtn.setReadPermissions("user_friends");
		loginBtn.setUserInfoChangedCallback(new UserInfoChangedCallback() {
			@Override
			public void onUserInfoFetched(GraphUser user) {
				if (user != null) {

				} else {

				}
			}
		});
		frame_forgotpassword = (FrameLayout) findViewById(R.id.frame_forgotpassword);
		edit_username = (EditText) findViewById(R.id.edit_username);
		edit_password = (EditText) findViewById(R.id.edit_password);

		btn_back = (Button) findViewById(R.id.button_back);
		btn_login = (Button) findViewById(R.id.button_login);
		btn_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent it = new Intent(LoginScreen.this, ChossingScreen.class);
				startActivity(it);
				finish();
			}
		});

		btn_login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				username = edit_username.getText().toString();
				password = edit_password.getText().toString();

				if (mConnectionDetector.isConnectingToInternet()) {
					LoginProcess();
				} else {
					showAlertDialog(LoginScreen.this, "Error",
							"No Internet Connection", false);

				}

			}
		});
		frame_forgotpassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent it = new Intent(LoginScreen.this, ForgotPassword.class);
				startActivity(it);
				finish();

			}
		});

	}

	private Session.StatusCallback statusCallback = new Session.StatusCallback() {

		@Override
		public void call(Session session, SessionState state,

		Exception exception) {

			if (state.isOpened()) {

				Log.d("MainActivity", "Facebook session opened.");

			} else if (state.isClosed()) {

				Log.d("MainActivity", "Facebook session closed.");

			}

		}

	};

	@Override
	public void onResume() {
		super.onResume();
		uiHelper.onResume();

	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		uiHelper.onDestroy();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);
		if (Session.getActiveSession().isOpened()) {
			// Request user data and show the results
			Request.executeMeRequestAsync(Session.getActiveSession(),
					new Request.GraphUserCallback() {

						@Override
						public void onCompleted(GraphUser user,
								Response response) {
							// TODO Auto-generated method stub
							if (user != null) {
								// Display the parsed user info

								try {
									// GraphObject graphObject = user
									// .getGraphObject();
									// Log.e("", "Response : " + response);

									JSONObject jsonObject = user
											.getInnerJSONObject();
									Log.e("", "Response : " + jsonObject);
									try {

										// JSONObject obj = jsonObject
										// .getJSONObject("state");
										// Log.e("", "Response : " + obj);
										Log.e("Id",
												"" + jsonObject.getString("id"));
										Log.e("NAME",
												""
														+ jsonObject
																.getString("name"));
										editor.putString("userid",
												jsonObject.getString("id"));
										editor.putString("username",
												jsonObject.getString("name"));
										editor.putString("fullname",
												jsonObject.getString("name"));
										editor.putString("email", "");
										// editor.putString("session",Session.getActiveSession());
										editor.commit();
										Intent it = new Intent(
												LoginScreen.this, Main.class);
										startActivity(it);
										finish();
									} catch (JSONException e) {

										e.printStackTrace();
									}

									// Log.e("", "UserID : " + user.getId());
									// Log.e("",
									// "User FirstName : "
									// + user.getFirstName());

								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}

					});
		}
	}

	@Override
	public void onSaveInstanceState(Bundle savedState) {
		super.onSaveInstanceState(savedState);
		uiHelper.onSaveInstanceState(savedState);

	}

	public class AsyncLoginTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// pDialog = new ProgressDialog(LoginScreen.this,
			// R.style.progress_dialog);
			//
			// pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			// pDialog.setCancelable(false);
			// pDialog.show();

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// pDialog.dismiss();

			JsonReponseParsing(result);
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("email", String
					.valueOf(params[0])));
			parameter.add(new BasicNameValuePair("password", String
					.valueOf(params[1])));

			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.Login(parameter);
			return result;
		}
	}

	public void JsonReponseParsing(String result) {
		JSONObject jObj;
		try {
			jObj = new JSONObject(result);

			Log.e("response", "" + result);
			if (jObj != null) {
				if (jObj.getBoolean("success")) {
					JSONObject object = jObj.getJSONObject("data");

					editor.putString("userid", object.getString("id"));
					editor.putString("username", username);
					editor.putString("fullname", (object.getString("firstname")
							+ " " + object.getString("lastname")));
					editor.putString("email", object.getString("email"));
					editor.commit();

					Intent it = new Intent(LoginScreen.this, Main.class);
					startActivity(it);
					finish();

				} else {

					showAlertDialog(LoginScreen.this, "WRONG",
							"Wrong username & password", false);

				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected boolean validation(String username_, String password_) {
		// TODO Auto-generated method stub
		if (username_.equals("") && password_.equals("")) {

			showAlertDialog(LoginScreen.this, "Error",
					"Please fill the fields", false);

			return false;
		}
		if (username_.equals("")) {

			showAlertDialog(LoginScreen.this, "Error", "Please enter Username",
					false);

			return false;
		}
		if (password_.equals("")) {
			showAlertDialog(LoginScreen.this, "Error", "Please enter Password",
					false);

			return false;
		}

		return true;
	}

	public void LoginProcess() {

		boolean receive = validation(username, password);
		if (receive) {
			new AsyncLoginTask().execute(username, password);
		}

	}

	public void showAlertDialog(Context context, String title, String message,
			boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}
