package com.slipperyslickproductiondesign.login;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.slipperyslickproductiondesign.R;
import com.slipperyslickproductiondesign.SplashActivity;
import com.slipperyslickproductiondesign.constant.ConnectionDetector;
import com.slipperyslickproductiondesign.constant.NetworkConnector;

public class ChangePassword extends Activity {

	Button btnback, btn_submitpassword;

	ConnectionDetector mConnectionDetector;
	String email, New, confirm;
	EditText oldpassword, newpassword, confirmpassword;
	ProgressDialog pDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.changepassword);
		Intent intent = getIntent();
		email = intent.getStringExtra("Email");
		mConnectionDetector = new ConnectionDetector(this);
		btn_submitpassword = (Button) findViewById(R.id.btn_submitpassword);
		btnback = (Button) findViewById(R.id.button_forgot_back);

		newpassword = (EditText) findViewById(R.id.editext_New_password);
		confirmpassword = (EditText) findViewById(R.id.editext_confirm_password);
		btnback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}

		});
		btn_submitpassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				New = newpassword.getText().toString();
				confirm = confirmpassword.getText().toString();
				if (New.equals("") || confirm.equals("")) {
					showAlertDialog(ChangePassword.this, "ERROR",
							"Fill All field", false);
				}

				else if (mConnectionDetector.isConnectingToInternet()) {

					if (New.equals(confirm)) {
						new AsyncChangePasswordTask().execute(email, confirm);

					}

					else {
						showAlertDialog(ChangePassword.this, "ERROR",
								"Password mismatch", false);
					}
				} else {
					showAlertDialog(ChangePassword.this, "ERROR",
							"No Internet connection", false);
				}
			}
		});

	}

	public class AsyncChangePasswordTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(ChangePassword.this,
					R.style.progress_dialog);

			pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pDialog.dismiss();

			JsonReponseParsing(result);
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> parameter = new LinkedList<NameValuePair>();

			parameter.add(new BasicNameValuePair("email", String
					.valueOf(params[0])));

			parameter.add(new BasicNameValuePair("password", String
					.valueOf(params[1])));

			Log.e("parameter", "" + parameter);
			String result = NetworkConnector.ChangePassword(parameter);
			return result;
		}

	}

	public void JsonReponseParsing(String result) {
		JSONObject jObj;
		try {
			jObj = new JSONObject(result);
			Log.e("response", "" + result);
			if (jObj != null) {
				if (jObj.getBoolean("success")) {

					// Intent intent = new Intent(ForgotPassword.this,
					// ConfirmCode.class);
					// intent.putExtra("Email", email);
					// intent.putExtra("Code",randomcode);
					// startActivity(intent);
					// finish();

					Intent it = new Intent(ChangePassword.this,
							ChossingScreen.class);
					startActivity(it);

					// close this activity
					finish();
				} else {

					showAlertDialog(ChangePassword.this, "ERROR",
							"Wrong Email", false);

				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showAlertDialog(Context context, String title, String message,
			boolean error) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();

					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
