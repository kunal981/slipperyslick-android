package com.slipperyslickproductiondesign.constant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

public class NetworkConnector {

	public static String Login(List<NameValuePair> nameValuePair) {
		String url = null;
		url = AllConstants.login;
		Log.e("url", "" + url);
		String jString = null;

		jString = SocketConnection.postJSONObject(url, nameValuePair);

		return jString;

	}

	public static String Register(List<NameValuePair> nameValuePair) {
		String url = null;
		url = AllConstants.Signup;
		Log.e("url", "" + url);
		String jString = null;

		jString = SocketConnection.postJSONObject(url, nameValuePair);

		return jString;

	}

	public static String ForgotPassword(List<NameValuePair> nameValuePair) {
		String url = null;
		url = AllConstants.ForgotPassword;
		Log.e("url", "" + url);
		String jString = null;

		jString = SocketConnection.postJSONObject(url, nameValuePair);

		return jString;

	}

	public static String ChangePassword(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = AllConstants.CHANGEPASSWORD;
		Log.e("url", "" + url);
		String jString = null;

		jString = SocketConnection.postJSONObject(url, nameValuePair);

		return jString;
	}

	public static String GetGenresong(List<NameValuePair> nameValuePair) {
		// TODO Auto-generated method stub
		String url = null;
		url = AllConstants.PAGINATION;
		Log.e("url", "" + url);
		String jString = null;

		jString = SocketConnection.postJSONObject(url, nameValuePair);

		return jString;
	}

	public static String GetGenre(List<NameValuePair> nameValuePair) {

		String url = null;
		url = AllConstants.GETGENRE;
		Log.e("url", "" + url);
		String jString = null;

		jString = SocketConnection.postJSONObject(url, nameValuePair);

		return jString;

	}

	public static String GetSearch(List<NameValuePair> nameValuePair)
			throws IOException {

		String url = null;
		url = AllConstants.SEARCH;
		Log.e("url", "" + url);
		String jString = null;

		jString = SocketConnection.getJSONObject(url);

		return jString;

	}

}
