package com.slipperyslickproductiondesign.constant;

public class AllConstants {

	public static final String login = "https://slipperyy.com/beta/api/apilogin";
	public static final String Signup = "https://slipperyy.com/beta/api/apiregister";
	public static final String ForgotPassword = "https://slipperyy.com/beta/api/apiforgotpassword";
	public static final String CHANGEPASSWORD = "https://slipperyy.com/beta/api/resetpassword";
	// public static final String GETGENRE =
	// "http://slipperyy.com/dev/api/getGenre/";
	public static final String GETGENRE = "https://slipperyy.com/beta/api/getgenre";

	public static final String SEARCH = " https://slipperyy.com/beta/api/search";

	public static final String ADVANCESEARCH = "http://slipperyy.com/dev/api/advanceSearch/";
	public static final String PAGINATION = "https://slipperyy.com/beta/api/pagination";
	public static final String TOPBEATS = "http://slipperyy.com/dev/api/topBeats/";

}
