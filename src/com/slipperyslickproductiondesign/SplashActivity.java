package com.slipperyslickproductiondesign;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;

import com.slipperyslickproductiondesign.fragments.Main;
import com.slipperyslickproductiondesign.login.ChossingScreen;

public class SplashActivity extends Activity {
	private static int SPLASH_TIME_OUT = 1000;
	SharedPreferences pref;
	Editor editor;
	String name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_main);
		pref = getApplicationContext().getSharedPreferences("MyPref", 0);
		editor = pref.edit();
		name = pref.getString("username", "");

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				if (name.equals("")) {
					Intent it = new Intent(SplashActivity.this,
							ChossingScreen.class);
					startActivity(it);

					finish();
				} else {

					Intent it = new Intent(SplashActivity.this, Main.class);
					startActivity(it);
					finish();

				}

			}
		}, SPLASH_TIME_OUT);

	}

}